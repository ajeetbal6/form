export interface User{
    name:string,
    password:string,
    contact:string,
    email:string,
    eid:string
    lname:string;
}